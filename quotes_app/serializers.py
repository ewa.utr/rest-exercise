from rest_framework import serializers
from .models import Quote

class QuoteSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('text')
        model = Quote